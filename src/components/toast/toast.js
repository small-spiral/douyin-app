import Vue from 'vue'
import toast from './toast.vue'

//实例化组件对象
let Toast = Vue.extend(toast);
let instance

//设置一个时间定时器
let timer = null;
//设置我们的参数
let toastMsg = (options)=>{
    if(!instance) {
        //创建一个实例
        instance = new Toast();
        //挂在到页面上面
        document.body.append(instance.$mount().$el);
    }
    instance.duration = 2000;
    if(typeof options === 'string') {
        instance.message = options;
    }else if(typeof options === 'object') {
        instance.type = options.type;
        instance.message = options.message;
        instance.duration = options.duration || 2000;
    }else {
        return
    }
    instance.show = true;
    timer = setTimeout(() => {
        instance.show = false;
        clearTimeout(timer);
        timer = null
    }, instance.duration)
}

export default toastMsg