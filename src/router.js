import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

import Home from './views/Home.vue'

const router = new Router({
    routes: [
        {
            path: '/',
            redirect: 'index'
            
        },
        {
            path: '/',
            name: 'home',
            component: Home,
            children: [
                {
                    path: '/index',
                    name: 'index',
                    component:()=>import('./views/index/index.vue'),
                    children: [
                        {
                            path: '/index',
                            name: 'index',
                            component: () => import('./components/index/VideoList.vue')
                        }
                    ]
                },
                {
                    path: '/follow',
                    name: 'follow',
                    component:()=>import('./views/follow/follow.vue')
                },
                {
                    path: '/me',
                    name: 'me',
                    component:()=>import('./views/me/me.vue')
                },
                {
                    path: '/msg',
                    name: 'msg',
                    component:()=>import('./views/msg/Msg.vue')
                },
            ]
        },
        {
            path: '/sign',
            name: 'sign',
            component:()=>import('./views/index/sign.vue')
        },
        {
            path: '/tpsign',
            name: 'tpsign',
            component:()=>import('./views/index/tpsign.vue')
        },
        {
            path: '/code',
            name: 'code',
            component:()=>import('./views/index/code.vue')
        },
        {
            path: '/toast',
            name: 'toast',
            component:()=>import('./components/toast/toast.vue')
        },
        {
            path: '/edit',
            name: 'edit',
            component:()=>import('./views/me/edit.vue')
        },
        {
            path: '/publish',
            name: 'publish',
            component:()=>import('./views/publish/Publish.vue')
        },
        {
            path: '/upload',
            name: 'upload',
            component:()=>import('./views/publish/Upload.vue')
        },
    ]
})

export default router