import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './static/iconfont/iconfont.css'
import './assets/CSS/reset.css'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'
import 'video.js/dist/video-js.css'
import 'vue-video-player/src/custom-theme.css'
//自定义弹框
import Toast from './components/toast/toast.js'
Vue.prototype.$toast = Toast

Vue.config.productionTip = false

Vue.use(VueAwesomeSwiper)



new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
